#!/usr/bin/python

from tkinter import Tk
from gui.main_window import App
from modules.tle_database_reader import TleDatabaseReader
from modules.plotter import Plotter
from modules.sat_logging import SatLogger


logger = SatLogger.get_logger()


def main():

    logger.info("Satellites started")

    root = Tk()
    tle_reader = TleDatabaseReader()
    tle_reader.connect_db()
    plotter = Plotter()

    app = App(root, tle_reader, plotter)
    app.mainloop()

    tle_reader.close_db()


if __name__ == "__main__":
    main()


# import datetime
# from skyfield.api import load, EarthSatellite
# import numpy as np
# import matplotlib.pyplot as plt
# import cartopy.crs as ccrs
# from cartopy.feature.nightshade import Nightshade

# date = datetime.datetime.utcnow()
# date_text = datetime.datetime.now().strftime('%a %b %d %Y %X')

# ts = load.timescale(builtin=True)


# TLE = """ISS (ZARYA)
# 1 25544U 98067A   19203.81086311  .00000606  00000-0  18099-4 0  9996
# 2 25544  51.6423 184.5274 0006740 168.1171 264.4057 15.50995519180787"""

# name, L1, L2 = TLE.splitlines()

# sat = EarthSatellite(L1, L2)

# # minutes = np.arange(0, 200, 0.05) # about two orbits
# minutes = np.linspace(0, 400, 5000)
# times   = ts.utc(2019, 7, 23, 0, minutes)

# geocentric = sat.at(times)
# subsat = geocentric.subpoint()

# fig = plt.figure(figsize=(10, 5), dpi=100)
# ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())

# ax.stock_img()
# ax.set_title(f'Night time shading for {date_text}')
# ax.add_feature(Nightshade(date, alpha=0.2))

# plt.scatter(subsat.longitude.degrees, subsat.latitude.degrees, 1.00, transform=ccrs.PlateCarree(),
#             color='red') #  {"size": 10, "symbol": "triangle-right", "line": {"width": 1, "color": "black"}})
# plt.show()

# import tkinter as Tk
# import datetime
# import sys
# from skyfield.api import load, EarthSatellite
# import numpy as np
# from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
# from matplotlib.figure import Figure
# import cartopy.crs as ccrs
# from cartopy.feature.nightshade import Nightshade


# root = Tk.Tk()

# date = datetime.datetime.utcnow()
# date_text = datetime.datetime.now().strftime('%a %b %d %Y %X')

# ts = load.timescale(builtin=True)


# TLE = """ISS (ZARYA)
# 1 25544U 98067A   22004.41454216  .00004617  00000+0  89252-4 0  9992
# 2 25544  51.6438  67.6122 0005443   4.7241 106.2763 15.49859893319781"""

# name, L1, L2 = TLE.splitlines()

# sat = EarthSatellite(L1, L2)

# # minutes = np.arange(0, 200, 0.05) # about two orbits
# minutes = np.linspace(0, 400, 8000)
# times   = ts.utc(2022, 1, 4, 11, minutes)

# geocentric = sat.at(times)
# subsat = geocentric.subpoint()

# fig = Figure(figsize=(12, 7), dpi=100)
# ax = fig.add_axes([0.01, 0.01, 0.98, 0.98],  projection=ccrs.PlateCarree())

# ax.stock_img()
# ax.set_title(f'Night time shading for {date_text}')
# ax.add_feature(Nightshade(date, alpha=0.2))

# ax.scatter(subsat.longitude.degrees, subsat.latitude.degrees, 0.5, alpha=0.4, transform=ccrs.PlateCarree(),
#             color='green', marker='.')

# canvas = FigureCanvasTkAgg(fig, master=root)
# canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

# button = Tk.Button(master=root, text='Quit', command=sys.exit)
# button.pack(side=Tk.BOTTOM)

# Tk.mainloop()


# importing whole module
# from tkinter import *
# from tkinter.ttk import *

# # importing strftime function to
# # retrieve system's time
# from time import strftime

# # creating tkinter window
# root = Tk()
# root.title('Clock')

# # This function is used to
# # display time on the label
# def time():
#     string = strftime('%H:%M:%S %p')
#     lbl.config(text = string)
#     lbl.after(1000, time)

# def time2():
#     string = strftime('%H:%M:%S %p')
#     lbl2.config(text = string)
#     lbl2.after(1000, time2)

# def say_hello():
#     print('hello')


# # Styling the label widget so that clock
# # will look more attractive
# lbl = Label(root, font = ('calibri', 40, 'bold'),
#             background = 'purple',
#             foreground = 'white')

# lbl2 = Label(root, font = ('calibri', 40, 'bold'),
#             background = 'blue',
#             foreground = 'white')


# # Placing clock at the centre
# # of the tkinter window
# lbl.pack(anchor = 'center')
# lbl2.pack(anchor='center')
# time()
# time2()
# say_hello()

# mainloop()

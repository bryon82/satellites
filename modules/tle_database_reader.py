import sys
import os
import logging
import mariadb

logger = logging.getLogger("satellites." + __name__)


class TleDatabaseReader:
    def __init__(self):
        self.conn = None

    def connect_db(self) -> None:

        # Connect to MariaDB
        try:
            conn = mariadb.connect(
                user=os.environ["DB_USER"],
                host=os.environ["DB_HOST"],
                port=int(os.environ["DB_PORT"]),
                database=os.environ["DB_NAME"],
            )
        except mariadb.Error as ex:
            logger.error("Error connecting to MariaDB Platform: %s", ex.msg)
            sys.exit(1)

        self.conn = conn

    def get_group_sats(self, group_name: str) -> list:
        group_sats = []
        cur = self.conn.cursor()

        cur.execute(
            "select sat \
            from sat_groups natural join tles \
            where group_name = ? \
            order by sat",
            (group_name,),
        )

        for (sat,) in cur:
            group_sats.append(sat)

        return group_sats

    def get_tles(self, sats: list) -> dict:

        tles = {}
        cur = self.conn.cursor()

        for sat in sats:
            cur.execute(
                "select sat, tle \
                from tles \
                where sat = ?",
                (sat,),
            )

            for (sat_name, tle) in cur:
                tles[sat_name] = tle

        return tles

    def close_db(self):
        self.conn.close()

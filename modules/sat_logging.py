import os
import logging

logger = logging.getLogger("satellites." + __name__)


class SatLogger:
    @staticmethod
    def get_logger() -> logging.Logger:
        log = logging.getLogger("satellites")
        log.setLevel(logging.INFO)

        time_string = "%m/%d/%Y %H:%M:%S %Z"
        formatter = logging.Formatter(
            fmt="{asctime} [{module}] {message}", datefmt=time_string, style="{"
        )

        log_dir = os.path.join(os.path.expanduser("~"), "log")
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        f_handler = logging.FileHandler(
            os.path.join(log_dir, "satellites.log"), mode="a"
        )
        f_handler.setLevel(logging.WARNING)
        f_handler.setFormatter(formatter)
        log.addHandler(f_handler)

        c_handler = logging.StreamHandler()
        c_handler.setLevel(logging.INFO)
        c_handler.setFormatter(formatter)
        log.addHandler(c_handler)
        return log

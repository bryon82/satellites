# pylint:disable=abstract-class-instantiated
import logging
import datetime
import cartopy.crs as ccrs
from cartopy.feature.nightshade import Nightshade
from matplotlib.figure import Figure
from skyfield.api import load, EarthSatellite
import numpy as np

logger = logging.getLogger("satellites." + __name__)


class Plotter:
    def __init__(self):
        fig = Figure(figsize=(12, 6), dpi=100)
        date = datetime.datetime.utcnow()
        axes = fig.add_axes([0, 0, 1, 1], projection=ccrs.PlateCarree())
        axes.stock_img()
        axes.add_feature(Nightshade(date, alpha=0.2))

        self.fig = fig
        self.date = date
        self.axes = axes

    # def plot_orbit(self, tles):

    #     date_text = datetime.datetime.now().strftime('%a %b %d %Y %X')

    #     t_s = load.timescale()
    #     # minutes = np.arange(0, 200, 0.05) # about two orbits
    #     minutes = np.linspace(0, 400, 8000)
    #     times = t_s.utc(2022, 1, 4, 11, minutes)
    #     for sat_name, tle in tles.items():
    #         line_1, line_2 = tle.splitlines()
    #         sat = EarthSatellite(line_1, line_2, sat_name)
    #         geocentric = sat.at(times)
    #         subsat = geocentric.subpoint()
    #         self.axes.scatter(
    #             subsat.longitude.degrees,
    #             subsat.latitude.degrees,
    #             0.5,
    #             alpha=0.4,
    #             transform=ccrs.PlateCarree(),
    #             color='green',
    #             marker='.')

    def plot_groundtrack(self, sat_name, tle):

        line_1, line_2 = tle.splitlines()
        sat = EarthSatellite(line_1, line_2, sat_name)
        orb_per = np.around(2 * np.pi / sat.model.no_kozai).astype(int)
        t_s = load.timescale()
        date = datetime.datetime.utcnow()
        minutes = np.linspace(date.minute, (3 * orb_per) + date.minute, orb_per * 200)
        rev_minutes = np.linspace(
            (-1 * orb_per) + date.minute, date.minute, orb_per * 200
        )
        spans = []
        spans.append(t_s.utc(date.year, date.month, date.day, date.hour, minutes))
        spans.append(t_s.utc(date.year, date.month, date.day, date.hour, rev_minutes))
        for times, color in zip(spans, ["blue", "gray"]):
            subsat = sat.at(times).subpoint()
            self.axes.scatter(
                subsat.longitude.degrees,
                subsat.latitude.degrees,
                s=0.1,
                alpha=0.2,
                transform=ccrs.PlateCarree(),
                color=color,
                marker=".",
            )

    def plot_sats(self, tles):

        time = load.timescale().now()
        for sat_name, tle in tles.items():
            line_1, line_2 = tle.splitlines()
            sat = EarthSatellite(line_1, line_2, sat_name)
            geocentric = sat.at(time)
            subsat = geocentric.subpoint()
            self.axes.plot(
                subsat.longitude.degrees,
                subsat.latitude.degrees,
                alpha=0.8,
                transform=ccrs.PlateCarree(),
                color="green",
                marker="o",
                markersize=5.0,
            )
            self.axes.annotate(
                sat_name,
                (subsat.longitude.degrees, subsat.latitude.degrees),
                transform=ccrs.PlateCarree(),
                textcoords="offset points",
                xytext=(-1.66 * len(sat_name), -12.0),
                fontsize="xx-small",
            )

    @staticmethod
    def get_orbital_period(sat):
        return 2 * np.pi / sat.model.no_kozai

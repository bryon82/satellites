import tkinter as tk
from tkinter import ttk
import os
import logging
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from resources.config import sat_group_names

logger = logging.getLogger("satellites." + __name__)


class App(tk.Frame):
    def __init__(self, master: tk.Tk, tle_reader, plotter):
        super().__init__(master)
        master.iconbitmap(
            os.path.join(
                os.path.dirname(__file__), "..", "resources", "comms_satellite.ico"
            )
        )

        self.master = master
        self.plotter = plotter
        self.tle_reader = tle_reader
        self.setup_menu()
        self.add_sats()
        self.add_groundtrack()

        canvas = FigureCanvasTkAgg(self.plotter.fig, master=self.master)
        canvas.get_tk_widget().pack(side="top", fill="both", expand=1)

        # lb_font = Font(size=8, family='Helvetica')

        # # create listbox object
        # listbox = tk.Listbox(self, height = 40,
        #                 width = 20,
        #                 activestyle = 'dotbox',
        #                 font = lb_font)

        # # Define the size of the window.
        # master.geometry('500x600')

        # # Define a label for the list.
        # label = tk.Label(self, text = "Satellites")

        # # insert elements by their
        # # index and names.
        # i = 1
        # for sat in reader.tles.keys():
        #     listbox.insert(i, sat)
        #     i += 1

        # # pack the widgets
        # label.grid(column=0, row=0)
        # listbox.grid(column=0, row= 1)

    def setup_menu(self) -> None:
        """Sets up the menu for the gui.

        Adds a menu with a file menu and help menu.

        Args:
            master (tk.Tk): Master Tk window
        """

        menu = tk.Menu()
        self.master.config(menu=menu)

        file_menu = tk.Menu(menu, tearoff="off")
        menu.add_cascade(label="File", menu=file_menu)
        # file_menu.add_command(label="FreeFlyer...", command=self.get_ff_dir)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=self.quit)

        help_menu = tk.Menu(menu, tearoff="off")
        menu.add_cascade(label="Help", menu=help_menu)
        # help_menu.add_command(label="How to use", command=self.show_how_to_use)
        help_menu.add_command(label="About", command=self.show_about)

    def add_sats(self):
        sats = self.tle_reader.get_group_sats(sat_group_names["NOAA"])
        sats.extend(self.tle_reader.get_group_sats(sat_group_names["Space Stations"]))
        print("NOAA:")
        for sat in sats:
            print(sat)
        tles = self.tle_reader.get_tles(sats)

        self.plotter.plot_sats(tles)

    def add_groundtrack(self):
        sats = self.tle_reader.get_group_sats(sat_group_names["NOAA"])
        print("NOAA 15 track")
        tles = self.tle_reader.get_tles(sats)

        self.plotter.plot_groundtrack("NOAA 19", tles["NOAA 19"])

    def show_about(self) -> None:
        """Shows new window with about info."""

        top = tk.Toplevel(self, borderwidth=1, relief="solid")
        top.title("About")
        top.wm_attributes("-toolwindow", "True")
        top.geometry("110x70")
        ttk.Label(top, text="Version: 1.0.0").pack()
        ttk.Button(top, text="OK", command=top.destroy).pack()
        top.focus_force()
